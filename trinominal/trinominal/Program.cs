using System;

namespace trinominal
{
    class Program
    {
        /// <summary>
        /// This project uses the trinomial tree to calculate the price of underlying, value of the option and Greeks' value.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.            
            TrinomialTree T = new TrinomialTree();// Build the trinomial tree to calculate the Option price.
            DiscountTree D = new DiscountTree();// Build discount tree to calculate the Option value.
            Greeks greeks = new Greeks();// Greeks class to calculate the Greeks' value

            // Define a new European Option
            European E = new European();
            E.S = 100;// Underlying price 
            E.K = 100;// Strike price
            E.q = 0.03;// Dividend payout rate
            E.r = 0.06;// Interest rate
            E.sigma = 0.2;// volatility
            E.steps = 3;// Steps
            E.t = 1;// Time
            E.Type = Option.PutCall.Call;// Call type
            double[,] ECall = D.FinalPayoff(T, E); // Option final value tree
            double[,] OptionPrice = T.OptionPrice(E.S, E.K, E.r, E.q, E.sigma, E.t, E.steps);// Underlying price tree
            Console.WriteLine("Option Price:");
            for (int j = 0; j <= 2 * E.steps; j++)
            {
                for (int i = 0; i <= E.steps; i++)
                {
                    Console.Write(Math.Round(OptionPrice[j, i], 2) + "\t");
                }
                Console.Write("\n");
            }
            Console.WriteLine("European Call Option Value:");
            for (int j = 0; j <= 2 * E.steps; j++)
            {
                for(int i = 0; i <= E.steps; i++)
                {
                    Console.Write(Math.Round(ECall[j, i], 2) + "\t");
                }
                Console.Write("\n");
            }
            Console.WriteLine("European Call Greeks: Delta:" + Math.Round(greeks.GetDelta(E), 4) + "\t"
                + "Gamma:" + Math.Round(greeks.GetGamma(E), 4) + "\n" + "Vega:" + Math.Round(greeks.GetVega(E), 4) + "\t"
                + "Theta:" + Math.Round(greeks.GetTheta(E), 4) + "\t" + "Rho:" + Math.Round(greeks.GetRho(E), 4));
            Console.Write("\n");

            // European Put Option
            E.Type = Option.PutCall.Put;
            double[,] EPut = D.FinalPayoff(T, E);
            Console.WriteLine("European Put Option Value:");
            for (int j = 0; j <= 2 * E.steps; j++)
            {
                for (int i = 0; i <= E.steps; i++)
                {
                    Console.Write(Math.Round(EPut[j, i], 2) + "\t");
                }
                Console.Write("\n");
            }
            Console.WriteLine("European Put Greeks: Delta:" + Math.Round(greeks.GetDelta(E), 4) + "\t"
                + "Gamma:" + Math.Round(greeks.GetGamma(E), 4) + "\n" + "Vega:" + Math.Round(greeks.GetVega(E), 4) + "\t"
                + "Theta:" + Math.Round(greeks.GetTheta(E), 4) + "\t" + "Rho:" + Math.Round(greeks.GetRho(E), 4));
            Console.Write("\n");
            
            // Define a new American Option
            American A = new American();
            A.S = 100;
            A.K = 100;
            A.q = 0.03;
            A.r = 0.06;
            A.sigma = 0.2;
            A.steps = 3;
            A.t = 1;

            // American Call Option
            A.Type = Option.PutCall.Call;
            double[,] ACall = D.FinalPayoff(T, A);
            Console.WriteLine("American Call Option Value:");
            for (int j = 0; j <= 2 * A.steps; j++)
            {
                for (int i = 0; i <= A.steps; i++)
                {
                    Console.Write(Math.Round(ACall[j, i], 2) + "\t");
                }
                Console.Write("\n");
            }
            Console.WriteLine("American Call Greeks: Delta:" + Math.Round(greeks.GetDelta(A), 4) + "\t"
                + "Gamma:" + Math.Round(greeks.GetGamma(A), 4) + "\n" + "Vega:" + Math.Round(greeks.GetVega(A), 4) + "\t"
                + "Theta:" + Math.Round(greeks.GetTheta(A), 4) + "\t" + "Rho:" + Math.Round(greeks.GetRho(A), 4));
            Console.Write("\n");

            // American Put Option
            A.Type = Option.PutCall.Put;
            double[,] APut = D.FinalPayoff(T, A);
            Console.WriteLine("American Put Option Value:");
            for (int j = 0; j <= 2 * A.steps; j++)
            {
                for (int i = 0; i <= A.steps; i++)
                {
                    Console.Write(Math.Round(APut[j, i], 2) + "\t");
                }
                Console.Write("\n");
            }
            Console.WriteLine("American Put Greeks: Delta:" + Math.Round(greeks.GetDelta(A), 4) + "\t"
                + "Gamma:" + Math.Round(greeks.GetGamma(A), 4) + "\n" + "Vega:" + Math.Round(greeks.GetVega(A), 4) + "\t"
                + "Theta:" + Math.Round(greeks.GetTheta(A), 4) + "\t" + "Rho:" + Math.Round(greeks.GetRho(A), 4));
            Console.Write("\n");
            Console.ReadKey();
        }

        /// <summary>
        /// Option class contains variables Underlying Price, Strike Price, Interest rate, Dividend payout ratio,
        /// volatility, Time, steps, and option type.
        /// And methods GetStockPrice, NodeValue, Payoff.
        /// </summary>
        public class Option
        {
            public double S { get; set; }
            public double K { get; set; }
            public double r { get; set; }
            public double q { get; set; }
            public double sigma { get; set; }
            public double t { get; set; }
            public int steps { get; set; }
            public enum PutCall
            {
                Call,
                Put,
            }

            public PutCall Type
            {
                get;
                set;
            }
            /// <summary>
            /// Use trinomial tree to calculate the stock price.
            /// </summary>
            /// <returns></returns>
            public virtual double[,] GetStockPrice()
            {
                TrinomialTree StockPrice = new TrinomialTree();
                double[,] x;
                x = StockPrice.OptionPrice(S, K, r, q, sigma, t, steps);
                return x;
            }
            /// <summary>
            /// Use the node value and node price to Calculate the final value, dependent on Option type
            /// </summary>
            /// <param name="value"> Node Value</param>
            /// <param name="NodePayoff"> Node Payoff with different option method</param>
            /// <returns></returns>
            public virtual double NodeValue(double value, double NodePayoff)
            {
                return 0;
            }
            /// <summary>
            /// Calculate the Payoff based on different Option type.
            /// </summary>
            /// <param name="S0"> Underlying price</param>
            /// <param name="K"> Strike price</param>
            /// <param name="type"> Option type</param>
            /// <returns></returns>
            public virtual double Payoff(double S0, double K, PutCall type)
            {
                switch (type)
                {
                    case PutCall.Call:
                        return Math.Max(0.0, S0 - K);

                    case PutCall.Put:
                        return Math.Max(0.0, K - S0);

                    default:
                        return 0.0;
                }
            }
        }
        /// <summary>
        /// European Option inherits from Option
        /// </summary>
        public class European : Option
        {
            public override double[,] GetStockPrice()
            {
                return base.GetStockPrice();
            }
            /// <summary>
            /// Return the value without comparing.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="NodePayoff"></param>
            /// <returns></returns>
            public override double NodeValue(double value, double NodePayoff)
            {
                return value;
            }

            public override double Payoff(double S0, double K, PutCall type)
            {
                return base.Payoff(S0, K, type);
            }


        }
        /// <summary>
        /// American inherits from Option class
        /// </summary>
        public class American : Option
        {
            public override double[,] GetStockPrice()
            {
                return base.GetStockPrice();
            }
            /// <summary>
            /// Calculate the American Node Value
            /// </summary>
            /// <param name="value"></param>
            /// <param name="NodePayoff"></param>
            /// <returns></returns>
            public override double NodeValue(double value, double NodePayoff)
            {
                return Math.Max(value, NodePayoff);
            }
            public override double Payoff(double S0, double K, PutCall type)
            {
                return base.Payoff(S0, K, type);
            }


        }
        /// <summary>
        /// Build a Trinomial Tree to calculate the option price
        /// </summary>
        public class TrinomialTree
        {  
            /// <summary>
            /// Calculate the option price
            /// </summary>
            /// <param name="S"> Underlying price</param>
            /// <param name="K"> Strike price</param>
            /// <param name="r"> interest rate</param>
            /// <param name="q"> Dividend payout rate</param>
            /// <param name="sigma"> Volatility</param>
            /// <param name="t"> Time</param>
            /// <param name="steps"> Steps</param>
            /// <returns> All option node price</returns>
            public double[,] OptionPrice(double S, double K, double r, double q, double sigma, double t, int steps)
            {
                double DeltaT = t / steps; // Delta T
                double DeltaX = sigma * Math.Sqrt(3 * DeltaT); //Delta X  
                double v = r - q - 0.5 * Math.Pow(sigma, 2); 
                double up = Math.Exp(DeltaX); //The factor Underlying price goes up
                double down = 1 / up; //The factor Underlying price goes down
                double proUp = 0.5 * (((Math.Pow(sigma, 2) * DeltaT + Math.Pow(v, 2) * Math.Pow(DeltaT, 2))
                    / Math.Pow(DeltaX, 2))
                    + v * DeltaT / DeltaX);// The probability the price goes up
                double proDown = 0.5 * (((Math.Pow(sigma, 2) * DeltaT + Math.Pow(v, 2) * Math.Pow(DeltaT, 2))
                    / Math.Pow(DeltaX, 2))
                    - v * DeltaT / DeltaX);// The probability the price goes down
                double proMid = 1 - (proDown + proUp);// The probability the price remians the same
                double[,] allOptionPrice = new double[2 * steps + 1, steps + 1];
                for(int i = 0; i <= steps; i++)
                {
                    for(int j = 0; j<= 2 * i; j++)
                    {
                        allOptionPrice[j + steps - i, i] = S * Math.Pow(up, Math.Max(0, i - j)) * Math.Pow(down, Math.Max(0, j - i));
                    }
                }
                return allOptionPrice;

            }
            
        }
        /// <summary>
        /// Use the discounttree to calculate the Option payoff
        /// </summary>
        public class DiscountTree
        {
            /// <summary>
            /// Use the option price from Trinomial tree and option type to calculate the value of Option.
            /// </summary>
            /// <param name="trinomial"> TrinomialTree class </param>
            /// <param name="option"> Different Option class</param>
            /// <returns> Option final payoff</returns>
            public double[,] FinalPayoff(TrinomialTree trinomial, Option option)
            {
                double DeltaT = option.t / option.steps;
                double DeltaX = option.sigma * Math.Sqrt(3 * DeltaT);
                double v = option.r - option.q - 0.5 * Math.Pow(option.sigma, 2);
                double up = Math.Exp(DeltaX);
                double down = 1 / up;
                double proUp = 0.5 * (((Math.Pow(option.sigma, 2) * DeltaT + Math.Pow(v, 2) * Math.Pow(DeltaT, 2)) / Math.Pow(DeltaX, 2)) + (v * DeltaT / DeltaX));
                double proDown = 0.5 * (((Math.Pow(option.sigma, 2) * DeltaT + Math.Pow(v, 2) * Math.Pow(DeltaT, 2))
                    / Math.Pow(DeltaX, 2)) - (v * DeltaT / DeltaX));
                double proMid = 1 - (proDown + proUp);

                double[,] optionPrice = new double[2 * option.steps + 1, option.steps + 1];
                double[] NodePrice = new double[2 * option.steps + 1];
                double[] NodeValue = new double[2 * option.steps + 1];
                double[,] FinalValue = new double[2 * option.steps + 1, option.steps + 1];
                optionPrice = trinomial.OptionPrice(option.S, option.K, option.r, option.q, option.sigma, option.t, option.steps);
                for (int i = 0; i <= 2 * option.steps; i++)
                {
                    NodePrice[i] = optionPrice[i, option.steps];
                    NodeValue[i] = option.Payoff(NodePrice[i], option.K, option.Type);
                    FinalValue[i, option.steps] = NodeValue[i];
                }

                for(int j = option.steps - 1; j >= 0; j--)
                {
                    for (int i = option.steps - j; i <= option.steps + j; i++)
                    {
                        FinalValue[i, j] = Math.Exp(-option.r * DeltaT) * 
                            (proUp * FinalValue[i - 1, j + 1] 
                            + proMid * FinalValue[ i, j + 1] 
                            + proDown * FinalValue[i + 1, j + 1]);
                        FinalValue[i, j] = option.NodeValue(FinalValue[i, j]
                            , option.Payoff(optionPrice[i, j], option.K, option.Type));
                    }
                }
                return FinalValue;
            }
        }
        /// <summary>
        /// Greeks class to calculte all greeks' value.
        /// </summary>
        public class Greeks
        {
            TrinomialTree TrinomialTree = new TrinomialTree();
            DiscountTree DiscountTree = new DiscountTree();
            /// <summary>
            /// Calculate the delta
            /// </summary>
            /// <param name="option">Option type</param>
            /// <returns> Delta value</returns>
            public double GetDelta(Option option)
            {
                double[,] FinalPayoff = DiscountTree.FinalPayoff(TrinomialTree, option);
                double[,] OptionPrice = TrinomialTree.OptionPrice(option.S, option.K, option.r, option.q, option.sigma, option.t, option.steps);
                double delta = (FinalPayoff[option.steps - 1, 1] - FinalPayoff[option.steps + 1, 1]) 
                    / (OptionPrice[option.steps - 1, 1] - OptionPrice[option.steps + 1, 1]);
                return delta;
            }
            /// <summary>
            /// Calculate the Gamma
            /// </summary>
            /// <param name="option">Option type</param>
            /// <returns> Gamma value</returns>
            public double GetGamma(Option option)
            {
                double[,] FinalPayoff = DiscountTree.FinalPayoff(TrinomialTree, option);
                double[,] OptionPrice = TrinomialTree.OptionPrice(option.S, option.K, option.r, option.q, option.sigma, option.t, option.steps);
                double gamma = ((FinalPayoff[option.steps - 1, 1] - FinalPayoff[option.steps, 1]) / (OptionPrice[option.steps - 1, 1] - OptionPrice[option.steps, 1])
                    - (FinalPayoff[option.steps, 1] - FinalPayoff[option.steps + 1, 1]) / (OptionPrice[option.steps, 1] - OptionPrice[option.steps + 1, 1]))
                    / (0.5 * (OptionPrice[option.steps - 1, 1] - OptionPrice[option.steps + 1, 1]));
                return gamma;
            }
            /// <summary>
            /// Calculate the Vega
            /// </summary>
            /// <param name="option"> Option tyoe</param>
            /// <returns> Vega value</returns>
            public double GetVega(Option option)
            {
                double Deltasigma = 0.001 * option.sigma;
                option.sigma = option.sigma + Deltasigma;
                double[,] upSigmaValue = DiscountTree.FinalPayoff(TrinomialTree, option);
                option.sigma = option.sigma - 2 * Deltasigma;
                double[,] downSigmaValue = DiscountTree.FinalPayoff(TrinomialTree, option);
                double vega = (upSigmaValue[option.steps, 0] - downSigmaValue[option.steps, 0]) / (2 * Deltasigma);
                option.sigma = option.sigma + Deltasigma;
                return vega;
            }
            /// <summary>
            /// Calculate the Rho
            /// </summary>
            /// <param name="option"> Option type</param>
            /// <returns> Rho value</returns>
            public double GetRho(Option option)
            {
                double Deltar = 0.001 * option.r;
                option.r = option.r + Deltar;
                double[,] uprValue = DiscountTree.FinalPayoff(TrinomialTree, option);
                option.r = option.r - 2 * Deltar;
                double[,] downrValue = DiscountTree.FinalPayoff(TrinomialTree, option);
                double rho = (uprValue[option.steps, 0] - downrValue[option.steps, 0]) / (2 * Deltar);
                option.r = option.r + Deltar;
                return rho;
            }
            /// <summary>
            /// Calculate the Theta
            /// </summary>
            /// <param name="option"> Option type</param>
            /// <returns> Theta value</returns>
            public double GetTheta(Option option)
            {
                double[,] FinalPayoff = DiscountTree.FinalPayoff(TrinomialTree, option);
                double theta = (FinalPayoff[option.steps, 2] - FinalPayoff[option.steps, 0]) / (2 * option.t / option.steps);
                return theta;
            }
        }
    }
}
